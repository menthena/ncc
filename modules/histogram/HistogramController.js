(function(){

	'use strict';

	angular.module('ncc.test-app')
		.controller('HistogramController', ['$scope', 'AnalyticsModel', '$location', function( $scope, AnalyticsModel, $location ) {

			var main = function(){
				$scope.getAnalytics();
			};

			$scope.getAnalytics = function(){

				// Adding demo data
				if( AnalyticsModel.list.length === 0 ) {
					for( var i = 0; i < 1000; i++ ) {
						AnalyticsModel.add({
							'date': '01.01.2015',
							'url': 'http://',
							'time': Math.round( Math.random() * 30 )
						});
					}
				}

				$scope.analytics = AnalyticsModel.list;
			};

			$scope.clearData = function(){
				AnalyticsModel.clear();
			};

			$scope.addData = function(){
				$location.path('add-data');
			};

			main();
			
		}]);

})();