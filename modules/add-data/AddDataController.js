(function(){

	'use strict';

	angular.module('ncc.test-app')
		.controller('AddDataController', ['$scope', 'AnalyticsModel', function( $scope, AnalyticsModel ) {

			var main = function(){
				$scope.analytics = [];
				$scope.minDataLength = 20;
				$scope.resetData();
			};

			$scope.resetData = function(){
				$scope.analyticsData = {
					'date': undefined,
					'url': 'http://',
					'time': 0
				};
			};

			$scope.submit = function( form ) {
				
				if( form.$valid ) {
					
					// Adding the data into the model
					$scope.analytics = AnalyticsModel.add({
						'date': $scope.analyticsData.date,
						'url': $scope.analyticsData.url,
						'time': $scope.analyticsData.time
					});

					// Resetting the form
					form.$submitted = false;

					// Showing the success message
					$scope.added = true;
					
				}
			};
			
			main();

		}]);

})();