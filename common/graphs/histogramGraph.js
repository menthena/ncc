(function(){

	'use strict';

	angular.module('ncc.test-app')
		.directive('histogramGraph', [function(){

					// Graph model
					var Graph = function( element, data ) {
						this.element = element;
						this.data = data;
					};

					Graph.prototype = {

						init: function(){
							this.d3element = this.element[0];
							this.groupedData = this.groupData(this.data);

							this.margin = {top: 20, right: 20, bottom: 30, left: 40};
							this.width = 960 - this.margin.left - this.margin.right;
							this.height = 500 - this.margin.top - this.margin.bottom;

							this.x = d3.scale.linear()
							    .range([0, this.width]);

							this.y = d3.scale.linear()
							    .range([this.height, 0]);

							this.xAxis = d3.svg.axis()
							    .scale(this.x)
							    .orient('bottom')
							    .ticks(this.groupedData.length);

							this.yAxis = d3.svg.axis()
							    .scale(this.y)
							    .orient('left');

							this.drawSvg();
							this.setDomain();
							this.drawAxes();
							this.drawBars();

						},

						toArr: function( obj ) {
							var temp = [];
							for( var i in obj ) {
								temp.push(obj[i]);
							}
							return temp;
						},

						groupData: function( data ) {
							return this.toArr(_.groupBy(data, 'time'));
						},

						drawSvg: function(){
							this.svg = d3.select(this.d3element).append('svg')
							    .attr('width', this.width + this.margin.left + this.margin.right)
							    .attr('height', this.height + this.margin.top + this.margin.bottom)
							  .append('g')
							    .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');							
						},

						setDomain: function(){
							this.x.domain([0, d3.max(this.groupedData, function(d) { return d[0].time; })]);
							this.y.domain([0, d3.max(this.groupedData, function(d) { return d.length; })]);

						},

						drawAxes: function(){
							this.svg.append('g')
							  .attr('class', 'x axis')
							  .attr('transform', 'translate(0,' + this.height + ')')
							  .call(this.xAxis)
							.append('text')
							  .attr('x', this.width)
							  .attr('y', 20)
							  .attr('dy', '.71em')
							  .style('text-anchor', 'end')
							  .text('Page response time (seconds)');

							this.svg.append('g')
							  .attr('class', 'y axis')
							  .call(this.yAxis)
							.append('text')
							  .attr('transform', 'rotate(-90)')
							  .attr('y', 6)
							  .attr('dy', '.71em')
							  .style('text-anchor', 'end')
							  .text('Number of items');
						},

						drawBars: function(){
							var _self = this;
							this.svg.selectAll('.bar')
							  .data(this.groupedData)
							.enter().append('rect')
							  .attr('class', 'bar')
							  .attr('x', function(d) { return _self.x(d[0].time); })
							  .attr('width', '20px')
							  .attr('y', function(d) { return _self.y(d.length); })
							  .attr('height', function(d) { return _self.height - _self.y(d.length); })
							  .attr('fill', '#F00');
						}

					};


			return {

				scope: {
					'graphData': '='
				},

				restrict: 'A',

				link: function( scope, element ) {

					// Initing the graph
					var histogramGraph = new Graph(element, scope.graphData);
					histogramGraph.init();
					
				}
			};

			
		}]);

})();