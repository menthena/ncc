(function(){
	
	'use strict';

	angular.module('ncc.test-app').config(['$routeProvider', function( $routeProvider ) {


      	$routeProvider
      		.when('/add-data', {
      			controller: 'AddDataController',
      			templateUrl: 'modules/add-data/add-data.html'
      		})
      		.when('/histogram', {
      			controller: 'HistogramController',
      			templateUrl: 'modules/histogram/histogram.html'
      		})
      		.otherwise({
      			redirectTo: '/histogram'
      		});

	}]);

})();