(function(){

	'use strict';

	angular.module('ncc.test-app')
		.factory('AnalyticsModel', [function(){

			var analytics = [];

			return {

				get list(){
					return analytics;
				},

				add: function( row ) {
					analytics.push(row);
					return this.list;
				},

				clear: function(){
					analytics = [];
				}

			};
			
		}]);

})();