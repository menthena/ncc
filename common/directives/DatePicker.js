(function(){

	'use strict';

	angular.module('ncc.test-app')
		.directive('datePicker', [function(){

			return {

				restrict: 'A',

				link: function( scope, element ) {

					$(element[0]).datepicker({
						 format: 'dd/mm/yyyy',
						 autoclose: true
					});

				}

			};

			
		}]);

})();